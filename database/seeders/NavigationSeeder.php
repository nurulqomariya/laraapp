<?php

namespace Database\Seeders;

use App\Models\Navigation;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class NavigationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $konfigurasi = Navigation::create([
            'name' => 'Konfigurasi',
            'url' => 'konfigurasi',
            'icon' => 'ti-settings',
            'main_menu' => null,
        ]);
        $konfigurasi->subMenus()->create([
            'name' => 'Role',
            'url' => 'konfigurasi/roles',
            'icon' => '',
        ]);
        $konfigurasi->subMenus()->create([
            'name' => 'Permissions',
            'url' => 'konfigurasi/permissions',
            'icon' => '',
        ]);

        $forum = Navigation::create([
            'name' => 'Forum',
            'url' => 'forum',
            'icon' => 'ti-comment',
            'main_menu' => null,
        ]);
        $forum->subMenus()->create([
            'name' => 'Komentar',
            'url' => 'forum/komentar',
            'icon' => 'agenda',
        ]);

        $jadwal = Navigation::create([
            'name' => 'Jadwal',
            'url' => 'jadwal',
            'icon' => 'ti-agenda',
            'main_menu' => null,
        ]);
        $jadwal->subMenus()->create([
            'name' => 'Kelas',
            'url' => 'jadwal/kelas',
            'icon' => '',
        ]);
    }
}
