<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default_user_value = [
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];

        DB::beginTransaction();
        try {
            //code...
            $admin = User::create(array_merge([
                'email' => 'admin@gmail.com',
                'name' => 'admin',
            ], $default_user_value));

            $staff = User::create(array_merge([
                'email' => 'staff@gmail.com',
                'name' => 'staff',
            ], $default_user_value));

            $siswa = User::create(array_merge([
                'email' => 'siswa@gmail.com',
                'name' => 'siswa',
            ], $default_user_value));

            $instruktur = User::create(array_merge([
                'email' => 'instruktur@gmail.com',
                'name' => 'instruktur',
            ], $default_user_value));

            $role_admin = Role::create(['name' => 'admin']);
            $role_staff = Role::create(['name' => 'staff']);
            $role_siswa = Role::create(['name' => 'siswa']);
            $role_instruktur = Role::create(['name' => 'instruktur']);

            $permission = Permission::create(['name' => 'read konfigurasi/roles']);
            $permission = Permission::create(['name' => 'create konfigurasi/roles']);
            $permission = Permission::create(['name' => 'update konfigurasi/roles']);
            $permission = Permission::create(['name' => 'delete konfigurasi/roles']);

            $permission = Permission::create(['name' => 'read konfigurasi/permissions']);

            $permission = Permission::create(['name' => 'read forum/komentar']);
            $permission = Permission::create(['name' => 'delete forum/komentar']);

            $permission = Permission::create(['name' => 'read jadwal/kelas']);
            $permission = Permission::create(['name' => 'create jadwal/kelas']);
            $permission = Permission::create(['name' => 'update jadwal/kelas']);
            $permission = Permission::create(['name' => 'delete jadwal/kelas']);

            Permission::create((['name' => 'read konfigurasi']));
            Permission::create((['name' => 'read forum']));
            Permission::create((['name' => 'read jadwal']));

            $role_admin->givePermissionTo('read konfigurasi/roles');
            $role_admin->givePermissionTo('create konfigurasi/roles');
            $role_admin->givePermissionTo('update konfigurasi/roles');
            $role_admin->givePermissionTo('delete konfigurasi/roles');
            $role_admin->givePermissionTo('read konfigurasi');

            $role_admin->givePermissionTo('read konfigurasi/permissions');

            $role_admin->givePermissionTo('read forum', 'read forum/komentar', 'delete forum/komentar');

            $role_admin->givePermissionTo('read jadwal', 'read jadwal/kelas', 'delete jadwal/kelas');

            $admin->assignRole('admin');
            $staff->assignRole('staff');
            $siswa->assignRole('siswa');
            $instruktur->assignRole('instruktur');

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }
}
